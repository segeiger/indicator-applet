AC_PREREQ(2.53)

AC_INIT([indicator-applet], [12.10.1])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR([src/indicator-applet.c])

AM_INIT_AUTOMAKE
AM_MAINTAINER_MODE
AM_SILENT_RULES([yes])

IT_PROG_INTLTOOL([0.35.0])
AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_C_O

LT_INIT([disable-static])

AC_SUBST(VERSION)

###########################
# Dependencies 
###########################

GTK_REQUIRED_VERSION=3.1
INDICATOR_REQUIRED_VERSION=0.3.92
LIBGNOME_PANEL_REQUIRED=3.36.0
APPLET_PKG=libgnome-panel
INDICATOR_PKG=indicator3-0.4

PKG_CHECK_MODULES(APPLET, gtk+-3.0 >= $GTK_REQUIRED_VERSION
                          x11
                          libido3-0.1
                          $APPLET_PKG >= $LIBGNOME_PANEL_REQUIRED
                          $INDICATOR_PKG >= $INDICATOR_REQUIRED_VERSION)
AC_SUBST(APPLET_CFLAGS)
AC_SUBST(APPLET_LIBS)

###########################
# Check to see if we're local
###########################

with_localinstall="no"
AC_ARG_ENABLE(localinstall, AS_HELP_STRING([--enable-localinstall], [install all of the files localy instead of system directories (for distcheck)]), with_localinstall=$enableval, with_localinstall=no)

###########################
# Indicator Info
###########################

if test "x$with_localinstall" = "xyes"; then
	MODULESDIR="${libdir}/gnome-panel/modules/"
	INDICATORDIR="${libdir}/indicators/2/"
	INDICATORICONSDIR="${datadir}/indicator-applet/icons/"
else
	MODULESDIR=`$PKG_CONFIG --variable=modulesdir $APPLET_PKG`
	INDICATORDIR=`$PKG_CONFIG --variable=indicatordir $INDICATOR_PKG`
	INDICATORICONSDIR=`$PKG_CONFIG --variable=iconsdir $INDICATOR_PKG`
fi

AC_SUBST(MODULESDIR)
AC_SUBST(INDICATORDIR)
AC_SUBST(INDICATORICONSDIR)

###########################
# Internationalization
###########################

GETTEXT_PACKAGE=indicator-applet
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [Name of the default gettext domain])

ALL_LINGUAS="af am an ar ast az be bg bn br bs ca ca@valencia crh csb cs cv cy da de dv el en_AU en_CA en_GB eo es et eu fa fi fr fur fy gl gv he hi hr hu hy id is it ja ka kk km kn ko ku la lb lt lv mk ml mr ms nb ne nl nn oc pa pl pt_BR pt ro ru sc sd si sk sl sq sr sv ta te th tr ug uk ur vec vi zh_CN zh_HK zh_TW"

AM_GLIB_GNU_GETTEXT

###########################
# Files
###########################

AC_OUTPUT([
Makefile
src/Makefile
data/Makefile
po/Makefile.in
])

###########################
# Results
###########################

AC_MSG_NOTICE([

Indicator Applet Configuration:

	Prefix:                 $prefix
])
