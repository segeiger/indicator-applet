/*
 * Copyright (C) 2020 Alberts Muktupāvels
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INDICATOR_APPLET_H
#define INDICATOR_APPLET_H

#include <libgnome-panel/gp-applet.h>

G_BEGIN_DECLS

#define INDICATOR_TYPE_APPLET (indicator_applet_get_type ())
G_DECLARE_DERIVABLE_TYPE (IndicatorApplet, indicator_applet,
                          INDICATOR, APPLET, GpApplet)

struct _IndicatorAppletClass
{
  GpAppletClass parent_class;

  const char * (* get_name)           (void);
  const char * (* get_description)    (void);

  const char * (* get_indicator_env)  (void);

  gboolean     (* load_module)        (const char *name);
  gboolean     (* load_indicator)     (const char *name);

  const char * (* get_hotkey_keycode) (void);

  const char * (* get_menu_xml)       (void);
};

G_END_DECLS

#endif
