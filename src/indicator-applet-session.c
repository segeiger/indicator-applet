/*
 * Copyright (C) 2020 Alberts Muktupāvels
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "indicator-applet-session.h"

#include <glib/gi18n-lib.h>

struct _IndicatorAppletSession
{
  IndicatorApplet parent;
};

G_DEFINE_TYPE (IndicatorAppletSession,
               indicator_applet_session,
               INDICATOR_TYPE_APPLET)

static const char *
indicator_applet_session_get_name (void)
{
  return _("Indicator Applet Session");
}

static const char *
indicator_applet_session_get_description (void)
{
  return _("A place to adjust your status, change users or exit your session.");
}

static const char *
indicator_applet_session_get_indicator_env (void)
{
  return "indicator-applet-session";
}

static gboolean
indicator_applet_session_load_module (const char *name)
{
  return g_strcmp0 (name, "libsession.so") == 0 ||
         g_strcmp0 (name, "libme.so") == 0;
}

static gboolean
indicator_applet_session_load_indicator (const char *name)
{
  return g_strcmp0 (name, "com.canonical.indicator.session") == 0 ||
         g_strcmp0 (name, "com.canonical.indicator.me") == 0;
}

static const char *
indicator_applet_session_get_hotkey_keycode (void)
{
  return "<Super>S";
}

static const char *
indicator_applet_session_get_menu_xml (void)
{
  const char *menu_xml;

  menu_xml = ""
    "<interface>"
    "  <menu id=\"indicator-session-menu\">"
    "    <section>"
    "      <item>"
    "        <attribute name=\"label\" translatable=\"yes\">_About</attribute>"
    "        <attribute name=\"action\">indicator-session.about</attribute>"
    "      </item>"
    "    </section>"
    "  </menu>"
    "</interface>";

  return menu_xml;
}

static void
indicator_applet_session_class_init (IndicatorAppletSessionClass *self_class)
{
  IndicatorAppletClass *applet_class;

  applet_class = INDICATOR_APPLET_CLASS (self_class);

  applet_class->get_name = indicator_applet_session_get_name;
  applet_class->get_description = indicator_applet_session_get_description;
  applet_class->get_indicator_env = indicator_applet_session_get_indicator_env;
  applet_class->load_module = indicator_applet_session_load_module;
  applet_class->load_indicator = indicator_applet_session_load_indicator;
  applet_class->get_hotkey_keycode = indicator_applet_session_get_hotkey_keycode;
  applet_class->get_menu_xml = indicator_applet_session_get_menu_xml;
}

static void
indicator_applet_session_init (IndicatorAppletSession *self)
{
  AtkObject *atk_object;

  atk_object = gtk_widget_get_accessible (GTK_WIDGET (self));
  atk_object_set_name (atk_object, "indicator-applet-session");
}
