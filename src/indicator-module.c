/*
 * Copyright (C) 2020 Alberts Muktupāvels
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <glib/gi18n-lib.h>
#include <libgnome-panel/gp-module.h>

#include "indicator-applet.h"
#include "indicator-applet-appmenu.h"
#include "indicator-applet-complete.h"
#include "indicator-applet-session.h"

static GpAppletInfo *
indicator_get_applet_info (const char *id)
{
  GpGetAppletTypeFunc type_func;
  const gchar *name;
  const gchar *description;
  GpAppletInfo *info;

  if (g_strcmp0 (id, "indicator") == 0)
    {
      type_func = indicator_applet_get_type;
      name = _("Indicator Applet");
      description = _("An indicator of something that needs your attention on the desktop");
    }
  else if (g_strcmp0 (id, "indicator-appmenu") == 0)
    {
      type_func = indicator_applet_appmenu_get_type;
      name = _("Indicator Applet Appmenu");
      description = _("A applet containing the application menus.");
    }
  else if (g_strcmp0 (id, "indicator-complete") == 0)
    {
      type_func = indicator_applet_complete_get_type;
      name = _("Indicator Applet Complete");
      description = _("A unified applet containing all of the indicators.");
    }
  else if (g_strcmp0 (id, "indicator-session") == 0)
    {
      type_func = indicator_applet_session_get_type;
      name = _("Indicator Applet Session");
      description = _("A place to adjust your status, change users or exit your session.");
    }
  else
    {
      g_assert_not_reached ();
      return NULL;
    }

  info = gp_applet_info_new (type_func, name, description, "indicator-applet");

  return info;
}

static const gchar *
indicator_get_applet_id_from_iid (const gchar *iid)
{
  if (g_strcmp0 (iid, "IndicatorAppletFactory::IndicatorApplet") == 0)
    return "indicator";
  else if (g_strcmp0 (iid, "IndicatorAppletAppmenuFactory::IndicatorAppletAppmenu") == 0)
    return "indicator-appmenu";
  else if (g_strcmp0 (iid, "IndicatorAppletCompleteFactory::IndicatorAppletComplete") == 0)
    return "indicator-complete";
  else if (g_strcmp0 (iid, "FastUserSwitchAppletFactory::FastUserSwitchApplet") == 0)
    return "indicator-session";

  return NULL;
}

void
gp_module_load (GpModule *module)
{
  bindtextdomain (GETTEXT_PACKAGE, LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  gp_module_set_gettext_domain (module, GETTEXT_PACKAGE);

  gp_module_set_abi_version (module, GP_MODULE_ABI_VERSION);

  gp_module_set_id (module, "org.ayatana.indicator-applet");
  gp_module_set_version (module, PACKAGE_VERSION);

  gp_module_set_applet_ids (module,
                            "indicator",
                            "indicator-appmenu",
                            "indicator-complete",
                            "indicator-session", NULL);

  gp_module_set_get_applet_info (module, indicator_get_applet_info);
  gp_module_set_compatibility (module, indicator_get_applet_id_from_iid);
}
